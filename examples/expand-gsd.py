# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Walkthrough: Deformation Rate Spectrum from Real World Grain Size Distribution

# %%
# %matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import minimize, NonlinearConstraint
from scipy.integrate import quad, quad_vec
import scipy.interpolate as intp
from scipy.special import gamma
import warnings


# %% [markdown]
# ## Helper Functions

# %% [markdown]
# ### `powspace`

# %% [markdown]
# This does the same as `numpy.logspace()`, but takes real start and end values instead of
# powers of a chosen base. In any case, the result is independent of the chosen base:
#
# $$
# x_n = x_\text{start}\,{\Bigl(\frac{x_\text{end}}{x_\text{start}}\Bigr)} ^ {\frac{n}{N - 1}}
# \quad,\quad
# n = 0, 1, \dots, N-1
# $$

# %%
def powspace(start: float, end: float, num: int = 50):
    """Numbers spaced evenly on a log scale.

    Parameters
    ----------
    start : float
        Start value of the sequence.
    end : float
        End value of the sequence.
    num : int
        Number of samples to generate.
    """
    if num < 2:
        raise ValueError(
            f'This function is meant to generate a range of several values, not {num=}.'
        )
    if start <= 0. or end <= 0.:
        raise ValueError(
            f'xlogspace produces log spaced values larger 0. Please choose start '
            f'and end appropriately. Got {start=} and {end=}.'
        )
    return start * (end / start) ** (np.arange(num) / (num - 1))


# %% [markdown]
# ### `handle_y_q`

# %%
def handle_y_q(func):
    """A decorator which handles the array and scalar combinations of arguments
    `y` and `q` as they appear in the base functions and their integrals.
    """
    def handle(y, q):
        y, q = np.asarray(y), np.asarray(q)
        if q.ndim > 1:
            raise ValueError(f'q should be a scalar or 1D array-like. Got {q.ndim=}.')
        return_scalar = y.ndim < 1 and q.ndim < 1
        unpack_last = y.ndim > 0 and q.ndim == 0
        # print(f"{return_scalar=}, {unpack_last=}")
        y = y[..., None]
        ret = func(y, q)
        if return_scalar:
            return ret[0]
        elif unpack_last:
            # print(ret)
            return ret[..., 0]
        else:
            return ret
    return handle


# %% [markdown]
# ## Kernels for Base Functions

# %% [markdown]
# Base functions can be split into two factors, a norming factor (subscript $_0$), and a 'kernel'
# $K(y)$ which depends only on the fraction $y=x/\lambda_l$.
# For example for $\nu_l(x)$:
#
# $$
# \nu_l(x) = \nu_{0,l}\,K_(y)
# $$
#
# The norming factors are relatively simple functions of $x, \lambda, q,\dots$ and do not depend
# on the chosen kernel base (Grady, Weibull, Log-Exp).

# %% [markdown]
# ### Grady Base

# %% [markdown]
# $$
# K_\text{G}(y) = \frac{1}{1 + y^q}
# $$

# %%
def kernel_grady(y, q):
    return 1 / (1 + y ** q)


# %% [markdown]
# $$
# \frac{\nu_l(x)}{\nu_{0,l}} = K_\text{G}(y)
# \quad,\quad
# y = \frac{x}{\lambda_l}
# $$

# %%
@handle_y_q
def grady_nu(y, q):
    return kernel_grady(y, q)


# %% [markdown]
# $$
# \frac{\sigma_l(x)}{\sigma_{0,l}} = y^2\,K_\text{G}(y)
# \quad,\quad
# y = \frac{x}{\lambda_l}
# $$

# %%
@handle_y_q
def grady_sig(y, q):
    return y ** 2 * kernel_grady(y, q)


# %% [markdown]
# $$
# \frac{\mu_l(x)}{\mu_{0,l}} = y^3\,K_\text{G}(y)
# \quad,\quad
# y = \frac{x}{\lambda_l}
# $$

# %%
@handle_y_q
def grady_mu(y, q):
    return y ** 3 * kernel_grady(y, q)


# %%
fig, ax = plt.subplots()

xp = np.logspace(-2, 2, 300)
ax.plot(xp, grady_sig(xp, 3))
# ax.plot(xp, xp ** 2)
ax.set(
    yscale='log', xscale='log'
);


# %% [markdown]
# $$
# I_{2,\text{G}}(q) = \int_0^\infty\!\!y^2\,K_\text{G}(y)\,dy
# $$

# %%
def grady_int3(q):
    if np.isscalar(q):
        ret = quad(lambda xx: grady_mu(xx, q), 0, np.inf)
    else:
        ret = quad_vec(lambda xx: grady_mu(xx, q), 0, np.inf)
    if ret[1] > 1e-8:
        warnings.warn(f"High error estimate ({ret[1]}) for {q=}.")
    return ret[0]


# %%
grady_int3(np.array([4, 4.1]))

# %%
fig, ax = plt.subplots(constrained_layout=True)

q = powspace(4.1, 10, 100)

ax.plot(q, grady_int3(q), label=r'$I_3(q)=\int_0^\infty \dfrac{y^3}{1 + y^q}\,dy$')
ax.set(
    # ylim=(.3, 1.2),
    yscale='log',
    xlim=(4, 10), xlabel=r'$q$', xscale='log'
)
ax.legend(loc='upper right')


# %% [markdown]
# ### Weibull Base

# %%
def weibull(x, lmda, q):
    y = x / lmda
    return (q / lmda) * y ** (q - 1) * np.exp(-(y ** q))


def weibull_cumul(x, lmda, q):
    return 1. - np.exp(-((x / lmda) ** q))


# %% [markdown]
# $$
# K_\text{W}(y) = y^{q-4}\,e^{-y^q}
# $$

# %%
def kernel_weibull(y, q):
    return y ** (q - 4) * np.exp(-(y ** q))


# %% [markdown]
# $$
# \frac{\nu_l(x)}{\nu_{0,l}} = K_\text{W}(y)
# \quad,\quad
# y = \frac{x}{\lambda_l}
# $$

# %%
@handle_y_q
def weibull_nu(y, q):
    return kernel_weibull(y, q)


# %% [markdown]
# $$
# \frac{\sigma_l(x)}{\sigma_{0,l}} = y^2\,K_\text{W}(y)
# \quad,\quad
# y = \frac{x}{\lambda_l}
# $$

# %%
@handle_y_q
def weibull_sig(y, q):
    return y ** 2 * kernel_weibull(y, q)


# %% [markdown]
# $$
# \frac{\mu_l(x)}{\mu_{0,l}} = y^3\,K_\text{W}(y)
# \quad,\quad
# y = \frac{x}{\lambda_l}
# $$

# %%
@handle_y_q
def weibull_mu(y, q):
    return y ** 3 * kernel_weibull(y, q)


# %%
fig, ax = plt.subplots()

xp = np.logspace(-2, 1, 300)
ax.plot(xp, weibull_sig(xp, 4))
ax.set(
    yscale='log', xscale='log',
    ylim=(1e-4, 1), xlim=(1e-2, 2)
);


# %% [markdown]
# $$
# I_3(q) = \int_0^\infty y^{q-1}e^{-y^q}dy = \frac{1}{q}
# $$

# %%
def weibull_int3(q):
    if np.isscalar(q):
        ret = 1 / q
    else:
        ret = 1 / np.asarray(q)
    return ret


# %% [markdown]
# ## Material Constants

# %%
m0 = 1.0
rho0 = 2.8e3

# %%
formfac_s = np.pi      # fs
formfac_v = np.pi / 6  # fv

# %% [markdown]
# ## Grain Size Data

# %%
data = np.loadtxt(
    '../data/Buettner-et-al_2006_JGR_grainsizes.txt',
    skiprows=2, delimiter=','
)

# %%
data

# %%
xsieve, ams, ast = 1e-3 * data[:, 0], data[:, 1], data[:, 2]

# %%
x = np.sqrt(xsieve[1:] * xsieve[:-1])
x

# %%
diff_m_ams = (ams[1:] - ams[:-1]) / 100
diff_m_ams

# %%
mu_ams = diff_m_ams / (xsieve[1:] - xsieve[:-1])
mu_ams

# %% [raw]
# diff_m_ast = (ast[1:] - ast[:-1]) / 100
# mu_ast = diff_m_ast / (xsieve[1:] - xsieve[:-1])
# mu_ast

# %% [markdown]
# ## Mode Length Scales

# %%
qq = 6
Lmax = 2 * len(x)

# %%
lmdas = powspace(x[0], x[-1], Lmax)[::-1]
lmdas


# %% [markdown]
# ## Norming Factors

# %%
def int3(base, q):
    base = base.lower()
    if base == 'grady':
        i3 = grady_int3(q)
    elif base == 'weibull':
        i3 = weibull_int3(q)
    return i3


# %% [markdown]
# $$
# \nu_{0,l} = \frac{m_0}{\rho_p\,I_3\,f_V\,\lambda_l^4}
# $$

# %%
def nu0s(base: str, lambdas, q, fs):
    i3 = int3(base, q)
    return m0 / (rho0 * i3 * formfac_v * lambdas ** 4)


# %% [markdown]
# $$
# \sigma_{0,l} = \frac{m_0}{\rho_p\,I_3}\frac{f_S}{f_V}\frac{1}{\lambda_l^2}
# $$

# %%
def sigma0s(base: str, lambdas, q):
    i3 = int3(base, q)
    return m0 * formfac_s / (rho0 * i3 * formfac_v * lambdas ** 2)


# %% [markdown]
# $$
# \mu_{0,l} = \frac{m_0}{I_3\,\lambda_l}
# $$

# %%
def mu0s(base: str, lambdas, q):
    i3 = int3(base, q)
    return m0 / (i3 * lambdas)


# %% [markdown]
# ## Base Functions

# %%
def handle_l_x(func):
    def broadcast(l, x):
        l, x = np.asarray(l), np.asarray(x)
        return_scalar = l.ndim < 1 and x.ndim < 1
        unpack_last = (x.ndim > 0) ^ (l.ndim > 0)
        x = x[..., None]
        ret = func(l, x)
        if return_scalar:
            return ret[0]
        elif unpack_last:
            return ret[..., 0]
        else:
            return ret
    return broadcast


# %% [markdown]
# $$
# \nu_l(x) = \nu_{0,l}\,K(x/\lambda_l)
# $$

# %%
def make_nu_l(base):
    nu0l = nu0s(base, lmdas, qq, formfac_s)
    if base.lower() == 'weibull':
        func = weibull_nu
    elif base.lower() == 'grady':
        func = grady_nu
    @handle_l_x
    def nu_l(l, x):
        return nu0l[l] * func(x / lmdas[l], qq)
    return nu_l


# %% [markdown]
# $$
# \sigma_l(x) = \sigma_{0,l}\,y^2K(x/\lambda_l)
# $$

# %%
def make_sig_l(base):
    sig0l = sigma0s(base, lmdas, qq)
    if base.lower() == 'weibull':
        func = weibull_sig
    elif base.lower() == 'grady':
        func = grady_sig
    @handle_l_x
    def sig_l(l, x):
        return sig0l[l] * func(x / lmdas[l], qq)
    return sig_l


# %% [markdown]
# $$
# \mu_l(x) = \mu_{0,l}\,y^3K(x/\lambda_l)
# $$

# %%
def make_mu_l(base):
    mu0l = mu0s(base, lmdas, qq)
    if base.lower() == 'weibull':
        func = weibull_mu
    elif base.lower() == 'grady':
        func = grady_mu
    @handle_l_x
    def mu_l(l, x):
        return mu0l * func(x / lmdas[l], qq)
    return mu_l


# %% [markdown]
# ## Interpolate the Grainsize Distribution

# %%
spl_ams = intp.CubicSpline(xsieve, ams / 100)
spl_ams_log = intp.CubicSpline(np.log(xsieve), np.log(ams / 100))


# %%
def amsfl(x):
    return np.exp(spl_ams_log(np.log(x)))


# %%
def amsfl_mu(x):
    lgx = np.log(x)
    return np.exp(spl_ams_log(lgx)) * spl_ams_log.derivative()(lgx) / x


# %%
ls = np.arange(Lmax)

# %%
ams

# %%
quad(amsfl_mu, xsieve[0], xsieve[-1])[0] + ams[0] / 100

# %% [markdown]
# ## Expansion Into Weibull Base

# %%
nu_l = make_nu_l('weibull')
sig_l = make_sig_l('weibull')
mu_l = make_mu_l('weibull')


# %%
def mu(x, a, q):
    assert np.isscalar(q), "mu: q needs to be a float scalar."
    return np.sum(a[None, :] * mu_l(ls, x), axis=1)


# %%
Kmax = 600

# %%
xk = powspace(x[0], x[-1], Kmax)
muk = amsfl_mu(xk)


# %%
def sq(a):
    muk = amsfl_mu(xk)
    return np.sum(np.log(mu(xk, a, qq) / muk) ** 2)


# %%
def ofunc(a):
    # sqa = sq(a)
    # ab = abs(a)
    return np.sum((mu(xk, a, qq) - muk) ** 2)


# %%
def dfdacond(a):
    """Absolute value of first difference (derivative). The maximum of left or
    right side is returned.
    """
    ret = np.empty_like(a)
    mean = a.mean()
    ret[[0, -1]] = np.abs(a[[1, -2]] - a[[0, -1]]) / mean
    ret[1:-1] = np.max(
        (np.abs(a[:-2] - a[1:-1]), np.abs(a[2:] - a[1:-1])),
        axis=0
    ) / mean
    return ret


# %%
def d2fdacond(a):
    """Absolute value of second difference (derivative)."""
    ret = np.empty_like(a)
    mean = a.mean()
    ret[[0, -1]] = 0.
    ret[1:-1] = np.abs(a[2:] - 2 * a[1:-1] + a[:-2]) / mean
    return ret


# %%
con1 = NonlinearConstraint(lambda x: x, 0, np.inf)  # a positive
con2 = NonlinearConstraint(dfdacond, 0., .33)      # upper bound for first difference
con3 = NonlinearConstraint(d2fdacond, 0, 0.2)

# %%
a = np.ones(Lmax)
res = minimize(
    fun=ofunc,
    x0=a,
    method='trust-constr',
    constraints=[con1, con2, con3],
    options=dict(maxiter=1000)
)

# %%
res['success']

# %%
res

# %%
lmdas

# %%
res['x']

# %%
fig, ax = plt.subplots(constrained_layout=True)

xp = powspace(x[0], x[-1], 300)

ax.plot(x, mu_ams, 's')
ax.plot(xp, amsfl_mu(xp), label='target')
ax.plot(xp, mu(xp, res['x'], qq), label='approx')
for l in range(Lmax):
    aa = res['x'][l]
    ax.plot(xp, aa * mu_l(l, xp), color='gray', lw=.33)
ax.vlines(
    lmdas, 0, 1, colors='gray', linestyles='dotted', lw=.33,
    transform=ax.get_xaxis_transform()
)

ax.set(
    xscale='log', yscale='log',
    ylim=(1e2, 1.2e3)
)
ax.legend(loc='upper right');

# %% [markdown]
# ### Map $\lambda$ to Deformation Rate

# %% [markdown]
# $$
# \lambda = {\biggl(\frac{\gamma}{\rho{\dot\varepsilon}^2}\biggr)}^{1/3}
# $$
#
# $\gamma$: Brittle energy per area

# %%
gamma_over_rho = 1e-2  # this thing is kinda tricy
epsdots = np.sqrt(gamma_over_rho / lmdas ** 3)
epsdots

# %%
lmdas

# %% [raw]
# plt.close('all')

# %%
a = res['x']

# %%
surf_c = m0 / (rho0 * lmdas[0])
surf_c

# %%
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(
    nrows=2, ncols=2, sharex='row',
    constrained_layout=True
)

ax1.plot(lmdas, a, 'o--')
ax1.set(
    xlabel=r'$\lambda$',
    ylabel=r'$a$', ylim=(0, None),
    xscale='log', yscale='linear'
)
ax2.plot(lmdas[::-1], np.cumsum(a[::-1]), 'o--')
ax2.set(xlabel=r'$\lambda\ /\ \mathrm{m}$')
ax3.plot(epsdots, surf_c * a, 'o--')
ax3.set(
    xscale='log', yscale='linear',
    ylim=(0, None),
    xlabel=r'$\dot{\varepsilon}\ /\ \mathrm{s^{-1}}$',
    ylabel=r'$S_c\,a\ /\ \mathrm{m^2}$'
)
ax4.plot(epsdots, np.cumsum(a) * surf_c, 'o--')
ax4.set(
    xscale='log', yscale='linear',
    ylim=(0, None),
    xlabel=r'$\dot{\varepsilon}\ /\ \mathrm{s^{-1}}$',
    # ylabel=r'$S_c\,a\ /\ \mathrm{m^2}$'
)
fig.set_size_inches(8, 8)

# %% [markdown]
# ## Expansion Into Grady Base

# %%
qq = 10

# %%
nu_l = make_nu_l('grady')
sig_l = make_sig_l('grady')
mu_l = make_mu_l('grady')


# %%
def mu(x, a, q):
    assert np.isscalar(q), "mu: q needs to be a float scalar."
    return np.sum(a[None, :] * mu_l(ls, x), axis=1)


# %%
Kmax = 600

# %%
xk = powspace(x[0], x[-1], Kmax)


# %%
def sq(a):
    muk = amsfl_mu(xk)
    return np.sum(np.log(mu(xk, a, qq) / muk) ** 2)


# %%
def ofunc(a):
    sqa = sq(a)
    # ab = abs(a)
    return sqa #+ ab / Lmax #+ a.mean()


# %%
def dfdacond(a):
    """Absolute value of first difference (derivative). The maximum of left or
    right side is returned.
    """
    ret = np.empty_like(a)
    mean = a.mean()
    ret[[0, -1]] = np.abs(a[[1, -2]] - a[[0, -1]]) / mean
    ret[1:-1] = np.max(
        (np.abs(a[:-2] - a[1:-1]), np.abs(a[2:] - a[1:-1])),
        axis=0
    ) / mean
    return ret


# %%
def d2fdacond(a):
    """Absolute value of second difference (derivative)."""
    ret = np.empty_like(a)
    mean = a.mean()
    ret[[0, -1]] = 0.
    ret[1:-1] = np.abs(a[2:] - 2 * a[1:-1] + a[:-2]) / mean
    return ret


# %%
con1 = NonlinearConstraint(lambda x: x, 0, np.inf)  # a positive
con2 = NonlinearConstraint(dfdacond, 0., .33)      # upper bound for first difference
con3 = NonlinearConstraint(d2fdacond, 0, 0.3)

# %%
a = np.ones(Lmax)
res = minimize(
    fun=ofunc,
    x0=a,
    method='trust-constr',
    constraints=[con1, con2, con3],
    options=dict(maxiter=1000)
)

# %%
res['success']

# %%
res

# %%
lmdas

# %%
res['x']

# %%
fig, ax = plt.subplots(constrained_layout=True)

xp = powspace(x[0], x[-1], 300)

ax.plot(x, mu_ams, 's')
ax.plot(xp, amsfl_mu(xp), label='target')
ax.plot(xp, mu(xp, res['x'], qq), label='approx')
for l in range(Lmax):
    aa = res['x'][l]
    ax.plot(xp, aa * mu_l(l, xp), color='gray', lw=.33)
ax.vlines(
    lmdas, 0, 1, colors='gray', linestyles='dotted', lw=.33,
    transform=ax.get_xaxis_transform()
)

ax.set(
    xscale='log', yscale='log',
    ylim=(1e2, 1.2e3)
)
ax.legend(loc='upper right');

# %% [markdown]
# ### Map $\lambda$ to Deformation Rate

# %% [markdown]
# $$
# \lambda = {\biggl(\frac{\gamma}{\rho{\dot\varepsilon}^2}\biggr)}^{1/3}
# $$
#
# $\gamma$: Brittle energy per area

# %%
gamma_over_rho = 1e-2  # this thing is kinda tricy
epsdots = np.sqrt(gamma_over_rho / lmdas ** 3)
epsdots

# %%
lmdas

# %% [raw]
# plt.close('all')

# %%
a = res['x']

# %%
surf_c = m0 / (rho0 * lmdas[0])
surf_c

# %%
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(
    nrows=2, ncols=2, sharex='row',
    constrained_layout=True
)

ax1.plot(lmdas, a, 'o--')
ax1.set(
    xlabel=r'$\lambda$',
    ylabel=r'$a$', ylim=(0, None),
    xscale='log', yscale='linear'
)
ax2.plot(lmdas[::-1], np.cumsum(a[::-1]), 'o--')
ax2.set(xlabel=r'$\lambda\ /\ \mathrm{m}$')
ax3.plot(epsdots, surf_c * a, 'o--')
ax3.set(
    xscale='log', yscale='linear',
    ylim=(0, None),
    xlabel=r'$\dot{\varepsilon}\ /\ \mathrm{s^{-1}}$',
    ylabel=r'$S_c\,a\ /\ \mathrm{m^2}$'
)
ax4.plot(epsdots, np.cumsum(a) * surf_c, 'o--')
ax4.set(
    xscale='log', yscale='linear',
    ylim=(0, None),
    xlabel=r'$\dot{\varepsilon}\ /\ \mathrm{s^{-1}}$',
    # ylabel=r'$S_c\,a\ /\ \mathrm{m^2}$'
)
fig.set_size_inches(8, 8)

# %%
