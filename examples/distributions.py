# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Distributions For Brittle Fragmenttion Modes

# %%
# %matplotlib inline
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad, quad_vec

# %%
from deformationrate_spectrum import powspace


# %% [markdown]
# ## Weibull

# %% [markdown]
# $$
# f(x) =
# \frac{1}{\lambda}{\Bigl(\frac{x}{\lambda}\Bigr)}^{q-1}
# \,e^{-{(x/\lambda)}^q}
# $$

# %%
def weibull_dist(x, lmda, q):
    return x ** (q - 1) / lmda ** q * np.exp(-(x / lmda) ** q)


# %%
def weibull_cumul(x, lmda, q):
    return 1 - np.exp(-(x / lmda) ** q)


# %% [markdown]
# ## Zoned Power (Grady)

# %% [markdown]
# $$
# f(x) = F_0 \frac{x^2}{1 + {\bigl(\frac{x}{\lambda}\bigr)}^q}
# \quad,\quad
# F_0^{-1} = \int_0^\infty\!\!\! \frac{x^2}{1 + {\bigl(\frac{x}{\lambda}\bigr)}^q} dx
# $$

# %%
def grady_dist(x, lmda, q, normed=False):
    normfac = 1.
    if normed:
        normfac = grady_cumul(np.inf, lmda, q, False)
    return x ** 2 / (1 + (x / lmda) ** q) / normfac


# %%
def grady_cumul(x, lmda, q, normed=False):
    if np.isscalar(x):
        x = np.asarray([x])
        scalar = True
    else:
        x = np.asarray(x)
        scalar = False
    ret = np.empty_like(x)
    ul = np.full_like(x, 3 * lmda)
    idx = x <= 3 * lmda
    if np.any(idx):
        ul[idx] = x[idx]
    for i, lim in enumerate(ul):
        # print(i, lim)
        ret[i] = quad(grady_dist, 0, lim, args=(lmda, q))[0]
    idx = ~idx
    if np.any(idx):
        for i in np.arange(len(ret))[idx]:
            ret[i] += quad(grady_dist, 3 * lmda, x[i], args=(lmda, q))[0]
    if normed:
        ret /= grady_cumul(np.inf, lmda, q, False)
    return ret[0] if scalar else ret


# %% [markdown]
# ## Log-Normal

# %% [markdown]
# $$
# f(x) = F_0\,e^{-{(q\,\ln(x/\lambda))}^2}
# \quad,\quad
# F_0^{-1} = \int_0^\infty e^{-{(q\ln(x/\lambda))}^2} dx
# $$

# %%
def lognorm_dist(x, lmda, q, normed=False):
    normfac = 1.
    if normed:
        normfac = lognorm_cumul(np.inf, lmda, q, False)
    return np.exp(-(q * np.log(x / lmda)) ** 2) / normfac


# %%
def lognorm_cumul(x, lmda, q, normed=False):
    if np.isscalar(x):
        x = np.asarray([x])
        scalar = True
    else:
        x = np.asarray(x)
        scalar = False
    ret = np.empty_like(x)
    ul = np.full_like(x, lmda)
    idx = x <= lmda
    if np.any(idx):
        ul[idx] = x[idx]
    for i, lim in enumerate(ul):
        # print(i, lim)
        ret[i] = quad(lognorm_dist, 0, lim, args=(lmda, q))[0]
    idx = ~idx
    if np.any(idx):
        for i in np.arange(len(ret))[idx]:
            ret[i] += quad(lognorm_dist, lmda, x[i], args=(lmda, q))[0]
    if normed:
        ret /= lognorm_cumul(np.inf, lmda, q, False)
    return ret[0] if scalar else ret


# %%
fig, (ax1, ax2) = plt.subplots(ncols=2, constrained_layout=True)

xp = powspace(1e-2, 1e2, 300)

ax1.plot(xp, weibull_cumul(xp, 1, 2), label=r'Weibull')
ax1.plot(xp, grady_cumul(xp, 1, 4, True), label=r'Grady')
ax1.plot(xp, lognorm_cumul(xp, 1, 1, True), label=r'Log-Normal')
ax1.set(
    xscale='log', yscale='log',
    ylim=(1e-2, 2), xlim=(1e-1, 1e2),
    title='Cumulative',
    xlabel=r'$x\ /\ \mathrm{m}$', ylabel=r'$F(x)$'
)
ax1.legend(loc='upper left')
ax2.plot(xp, weibull_dist(xp, 1, 2), label=r'Weibull')
ax2.plot(xp, grady_dist(xp, 1, 4, True), label=r'Grady')
ax2.plot(xp, lognorm_dist(xp, 1, 1, True), label=r'Log-Normal')
ax2.set(
    xscale='log', yscale='log',
    ylim=(1e-3, 1), xlim=(1e-2, 1e2),
    title='Density',
    xlabel=r'$x\ /\ \mathrm{m}$', ylabel=r'$f(x)$'
)
ax2.legend(loc='upper right')
fig.set_size_inches(10, 5)

# %% [raw]
# fig.savefig('../../graphs/distributions.pdf', dpi=300)
# fig.savefig('../../graphs/distributions.png', dpi=300)
