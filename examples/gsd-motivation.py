# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
# # %matplotlib inline
# %matplotlib widget
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy.interpolate as intp

# %% [markdown]
# ## Grain Size Data

# %%
data = np.loadtxt(
    '../data/Buettner-et-al_2006_JGR_grainsizes.txt',
    skiprows=2, delimiter=','
)

# %%
data

# %%
xsieve, ams, ast = 1e-3 * data[:, 0], data[:, 1], data[:, 2]

# %% [markdown]
# The dataset indicates cumulative masses that *passed* through a sieve of a stated size.
# The first mass (between 1 and 2%) is the finest fraction which contains all finer parts.
#
# - `xsieve`: Sieve sizes.
# - `ams`: AMS relative cumulative masses (%)
# - `ast`: AST relative cumulative masses (%)
#
# Locations of the centers of the sieve intervals on a log scale:

# %%
x = np.sqrt(xsieve[1:] * xsieve[:-1])
x

# %% [markdown]
# Per sieve interval relative masses (percent factor removed)

# %%
diff_m_ams = (ams[1:] - ams[:-1]) / 100
diff_m_ast = (ast[1:] - ast[:-1]) / 100
diff_m_ams, diff_m_ast

# %%
mu_ams = diff_m_ams / (xsieve[1:] - xsieve[:-1])
mu_ams

# %% [markdown]
# Average derivative of cumulative mass distribution in the `xsieve` intervals

# %%
mu_ast = diff_m_ast / (xsieve[1:] - xsieve[:-1])
mu_ast

# %%
fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True, constrained_layout=True)

ax1.plot(xsieve, ast, marker='s', mfc='none', label=r'AST')
ax1.plot(xsieve, ams, marker='x', label='AMS')
ax1.set(
    yscale='log', ylabel=r'mass \%'
)
ax1.legend(loc='lower right')

ax2.plot(x, mu_ast, marker='s', mfc='none', label='AST')
ax2.plot(x, mu_ams, marker='x', label='AMS')
ax2.set(
    xscale='log', yscale='log',
    xlabel=r'grain size / m'
)
ax2.legend(loc='upper right')
fig.set_size_inches(6, 7)


# %% [markdown]
# ## Continuous (Weibull) Distribution

# %% [markdown]
# Mass distribution (for a relative distribution, $m_0=1$)
#
# $$
# \mu(x) = m_0 \frac{q}{\lambda}{\Bigl(\frac{x}{\lambda}\Bigr)}^{q-1} e^{-(q/\lambda)^q}
# $$

# %% [markdown]
# Cumulative distribution
#
# $$
# m(x) = \int_0^x\!\!\mu(z)\, dz = m_0\,\bigl( 1 - e^{-(x/\lambda)^q} \bigr)
# $$

# %%
def weibull(x, lmda, q):
    y = x / lmda
    return (q / lmda) * y ** (q - 1) * np.exp(-(y ** q))


def weibull_cumul(x, lmda, q):
    return 1. - np.exp(-((x / lmda) ** q))


# %% [markdown]
# # Fit Continuous Distribution to Discrete Data

# %%
res_ast = curve_fit(weibull, x, mu_ast, p0=[5e-4, 1.2])
res_ast

# %% [markdown]
# A fit with log-weighted y-axis does not look all that different

# %%
res_log_ast = curve_fit(
    lambda x, lm, q: np.log(weibull(x, lm, q)),
    x,
    np.log(mu_ast),
    p0=[1e-4, 1.0],
    bounds=((0, 0.5), (5e-3, 10))
)
res_log_ast

# %%
res_ams = curve_fit(weibull, x, mu_ams, p0=[5e-4, 1.2])
res_ams

# %% [markdown]
# Best fit to data at a fixed $q=4$

# %%
res_4 = curve_fit(
    lambda x, lmda: weibull(x, lmda, 4), x, mu_ams, p0=[5e-4,])
res_4

# %%
lmda0 = 1.25e-4 / .75 ** .25
lmda0

# %% [markdown]
# ### ... and Plot the Results

# %%
lmda0


# %%
def format_ltx(flt: float, decimals: int = 4) -> str:
    p = int(np.floor(np.log10(flt)))
    fac = flt / 10 ** p
    ret = [
        ("{fac:." + f"{decimals - 1}" + "f}").format(fac=fac),
        r"\times10^{_p_}".replace("_p_", str(p))
    ]
    return "".join(ret)


# %%
fig, ax = plt.subplots()

xp = np.linspace(x[0], x[-1], 300)

l, = ax.plot(x, mu_ast, marker='s', mfc='none', label='AST')
ax.plot(
    xp, weibull(xp, *res_ast[0]), ls='dashed', color=l.get_color(),
    label=r'best fit: $\lambda=%s\,\mathrm{m}$, $q=%.2f$' % (
        format_ltx(res_ast[0][0], 3), res_ast[0][1])
)
l, = ax.plot(x, mu_ams, marker='x', label='AMS')
ax.plot(
    xp, weibull(xp, *res_ams[0]), ls='dashed', color=l.get_color(),
    label=r'best fit: $\lambda=%s\,\mathrm{m}$, $q=%.2f$' % (
        format_ltx(res_ams[0][0], 3), res_ams[0][1])
)
ax.plot(
    xp, weibull(xp, res_4[0][0], 4), ls='dotted', color=l.get_color(),
    label=r'$\lambda=%s\,\mathrm{m}$, best fit for $q=4$' % format_ltx(res_4[0][0], 3)
)
ax.plot(
    xp, weibull(xp, lmda0, 4), ls='dotted', color='k',
    label=r'$q=4$ example at $\lambda=%s\,\mathrm{m}$' % format_ltx(lmda0, 3)
)
ax.set(
    xscale='log', yscale='log', ylim=(2.25e2, 1.2e4), #aspect=1,
    xlabel=r'grain size / m',
    ylabel=r'$\left.\dfrac{\mu}{m_0}=\dfrac{1}{m_0}\dfrac{d\,m(x)}{dx}\quad\right/\mathrm{m^{-1}}$',
)
ax.grid(which='major', axis='both', ls='dotted', lw=.33)
ax.grid(which='minor', axis='both', ls='dotted', lw=.17)
ax.legend(loc='upper right')
fig.set_size_inches(8, 6.)
fig.tight_layout(pad=1)

# %% [markdown]
# ## Interpolate Between Measured Points

# %% [markdown]
# If a continuous function passes through each (discerete) point of the measured, cumulative mass distribution,
# its derivative, when averaged over the sieve intervals matches the measured $\mu$ values.
#
# A cubic spline is the lowest order spline that has two continuous derivatives. Its first derivative is a
# second order spline. This is a suitable method to interpolate the mass distribution.

# %% [markdown]
# ### Interpolate the AMS Dataset

# %% [markdown]
# - `spl_ams`: cubic spline through sieve sizes and cumulative mass dist.
# - `spl_ams_log`: cubic spline through log of sieve sizes and log of mass dist. This is a function
#   of $\ln(x)$, not $x$.

# %%
spl_ams = intp.CubicSpline(xsieve, ams / 100)
spl_ams_log = intp.CubicSpline(np.log(xsieve), np.log(ams / 100))


# %% [markdown]
# `spl_ams_log()`:  
# Recover the cumulative mass distribution from the log spline. If the spline was of order 1, the
# resulting function would show up as piece-wise linear on a log-log plot. Here it shows up as
# piece-wise order 3 polynomials on a log-log plot (which is a little harder to identify).

# %%
def spl_ams_fromlog(x):
    return np.exp(spl_ams_log(np.log(x)))


# %%
def spl_ams_log_mu(x):
    lgx = np.log(x)
    return np.exp(spl_ams_log(lgx)) * spl_ams_log.derivative()(lgx) / x


# %% [markdown]
# ### Interpolate the AST Dataset

# %%
spl_ast = intp.CubicSpline(xsieve, ast / 100)
spl_ast_log = intp.CubicSpline(np.log(xsieve), np.log(ast / 100))


# %%
def astfl(x):
    return np.exp(spl_ast_log(np.log(x)))


# %%
def astfl_mu(x):
    lgx = np.log(x)
    return np.exp(spl_ast_log(lgx)) * spl_ast_log.derivative()(lgx) / x


# %% [markdown]
# ### Show the Distributions

# %%
fig, (ax1, ax2) = plt.subplots(ncols=2, sharex=True, constrained_layout=True)

xp = np.logspace(np.log10(xsieve[0]), np.log10(xsieve[-1]), 300)

l1, = ax1.plot(xsieve, ams / 100, marker='s', mfc='none', label='AMS', lw=0)
l2, = ax1.plot(xsieve, ast / 100, marker='s', mfc='none', label='AST', lw=0)
ax1.plot(xp, spl_ams(xp), ls='dashed', color=l1.get_color(), lw=.5)
ax1.plot(xp, spl_ast(xp), ls='dashed', color=l2.get_color(), lw=.5)
ax1.plot(xp, spl_ams_fromlog(xp), ls='dotted', color=l1.get_color())
ax1.plot(xp, astfl(xp), ls='dotted', color=l2.get_color())
ax1.vlines(
    xsieve, 0, 1,
    colors='gray', linestyles='dotted', lw=.5,
    transform=ax1.get_xaxis_transform()
)
ax1.set(
    xscale='log', yscale='log',
    xlabel=r'$x\ /\mathrm{m}$', ylabel=r'$m(x) / m_0$'
)
ax1.legend(loc='lower right')
ax2.hlines(
    mu_ams, xsieve[:-1], xsieve[1:],
    colors=l1.get_color(), lw=.5, zorder=-1
)
ax2.plot(xp, spl_ams.derivative()(xp), ls='dashed', lw=.5)
ax2.plot(xp, spl_ams_log_mu(xp), ls='dotted', color=l1.get_color())
ax2.plot(xp, spl_ast.derivative()(xp), ls='dashed', lw=.5)
ax2.plot(xp, astfl_mu(xp), ls='dotted', color=l2.get_color())
ax2.hlines(
    mu_ast, xsieve[:-1], xsieve[1:],
    colors=l2.get_color(), lw=.5, zorder=-1
)
ax2.vlines(
    xsieve, 0, 1,
    colors='gray', linestyles='dotted', lw=.5,
    transform=ax2.get_xaxis_transform()
)
ax2.set(
    xscale='log', yscale='log',
    xlabel=r'$x\ /\mathrm{m}$',
    ylabel=r'$\dfrac{\mu}{m_0} / \mathrm{m^{-1}}$'
)
fig.set_size_inches(9, 4)

# %%
