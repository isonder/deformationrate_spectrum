# Examples

**work in progress ...**

These examples are meant to help motivate and speed up unederstanding of the library.
Note that the Jupyter notebooks (`*.ipynb`) are versioned only now and then. The latest versions are
available in the `*.py` files which are created usising a Jupyter Jupytext extension. They are essentially
Jupyter Notebooks with removed output cells, and converted to pure Python scripts. This way the content
is easier in version control.
