# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
# # %matplotlib inline
# %matplotlib widget
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sbn
from scipy.optimize import curve_fit

# %%
from deformationrate_spectrum import *

# %%
pltt = sbn.color_palette([c['color'] for c in plt.rcParams['axes.prop_cycle']])
pltt


# %%
def weibull_dist(x, lmda, q):
    return x ** (q - 1) / lmda ** q * np.exp(-(x / lmda) ** q)


# %%
def weibull_cumul(x, lmda, q):
    return 1 - np.exp(-(x / lmda) ** q)


# %%
lmda0 = 5e-4
q0 = 1.2

# %%
fig, (ax1, ax2) = plt.subplots(
    ncols=2, sharex=True,
    constrained_layout=True
)

xp = powspace(1e-7, 2e-3, 300)

ax1.plot(xp, weibull_cumul(xp, lmda0, q0), '--', color=pltt[0])
ax1.set(
    xscale='log', yscale='log',
    xlabel=r'$x\ /\ \mathrm{m}$', ylabel=r'$m/m_0$'
)
# ax1.legend(loc='lower right')
ax2.plot(xp, weibull_dist(xp, lmda0, q0), color=pltt[0])
ax2.set(
    xscale='log', yscale='log',
    xlabel=r'$x\ /\ \mathrm{m}$', ylabel=r'$\mu/m_0\ /\ \mathrm{m^{-1}}$'
)
# ax2.legend(loc='lower left')
fig.set_size_inches(8, 4)

# %%
find_opts = dict(maxiter=10000, gtol=1e-40, xtol=1e-40, barrier_tol=1e-40, verbose=1)

# %%
q2 = 4

# %%
exbase = WeibullBase(
    q=q2,
    m0=1.,
    rho=2.8e3,
    lmbmin=1e-5,
    lmbmax=2e-3,
    nterms=20
)

# %%
spec = Spectrum(
    target=lambda x: weibull_dist(x, lmda0, q0),
    base=exbase,
    xmin=1e-5,
    xmax=2e-3,
)
spec.aconstr1 = 25
spec.aconstr2 = 12.5

# %%
res = spec.find_spectrum(options=find_opts)

# %%
res['tr_radius']

# %%
res

# %%
exbase.a_s.sum()

# %%
pc = plt.cm.Paired
pc


# %%
def power(x, a, b):
    return a * x ** b


# %%
def model1(x, a, lmda, q):
    return a * weibull_dist(x, lmda, q)


# %% [raw]
# q2 / q0 - .5

# %%
afit = curve_fit(
    model1,
    exbase.lmdas, exbase.a_s,
    bounds=((0, 0, 2.07), (1e-2, 8.5e-4, 4.))
)
afit

# %%
exbapr = WeibullBase(
    q=q2,
    rho=2.8e3,
    lmbmin=1e-5,
    lmbmax=2e-3,
    nterms=20
)
exbapr.a_s = model1(exbapr.lmdas, *afit[0])

# %%
fig, (ax1, ax2) = plt.subplots(ncols=2, constrained_layout=True)

xp = powspace(1e-5, 2e-3, 300)
lp = powspace(1e-5, 2e-3, 200)

ax1.plot(xp, weibull_dist(xp, lmda0, q0), color=pc(1), ls='dotted', lw=1, label=r'target')
ax1.plot(xp, exbase.mu(xp), label='approx')
# ax1.plot(xp, exbapr.mu(xp), label='approx2')
ax1.set(
    xscale='log', yscale='log',
    xlabel=r'$x\ /\ \mathrm{m}$',
    ylabel=r'$\dfrac{\mu}{m_0}\ /\ \mathrm{m^{-1}}$'
)
ax1.legend(loc='lower left')
ax2.plot(exbase.lmdas, spec.a, 'o-', lw=1, ms=4, color=pc(1))
ax2.plot(lp, model1(lp, *afit[0]), color='gray', ls='dashed', lw=.5)
ax2.annotate(
    r'$%.1e\Bigl(\frac{x}{%.2f\,\mathrm{mm}}\Bigr)^{%.2f}\,e^{-(\lambda/%.2f\,\mathrm{mm})^{%.2f}}$' % (
        afit[0][0], afit[0][1] * 1e3, afit[0][2] - 1, afit[0][1] * 1e3, afit[0][2]
    ),
    xy=(1.9e-3, 3e-3), xytext=(1.5e-3, 5e-3),
    ha='right', fontsize=8,
    arrowprops=dict(arrowstyle='-', color='gray')
)
ax2.set(
    xscale='log', yscale='log',
    xlabel=r'$\lambda\ /\ \mathrm{m}$',
    ylabel=r'$a$',
    # ylim=(8e-4, None)
)
# ax2.legend(loc='upper left')
fig.set_size_inches(8, 4)

# %%
fig, (ax1, ax2, ax3) = plt.subplots(nrows=3, sharex=True, constrained_layout=True)

xp = powspace(1e-5, 2e-3, 300)

ax1.plot(xp, exbase.nu(xp))
ax1.set(
    xscale='log', yscale='linear',
    ylabel=r'$\nu\ /\ \mathrm{m^{-1}}$',
    ylim=(0, None)
)
ax2.plot(xp, exbase.sig(xp))
ax2.set(
    yscale='linear',
    ylabel=r'$\sigma\ /\ \mathrm{m^2/m}$',
    ylim=(0, None)
)
ax3.plot(xp, exbase.mu(xp))
ax3.set(
    yscale='linear',
    ylabel=r'$\mu\ /\ \mathrm{kg/m}$',
    xlabel=r'$x\ /\ \mathrm{m}$',
    ylim=(0, 1.3e3)
)
fig.set_size_inches(6, 9)

# %%
