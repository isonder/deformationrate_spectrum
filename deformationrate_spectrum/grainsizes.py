from typing import Union, Callable
from pathlib import Path
import numpy as np
from numpy.typing import NDArray, ArrayLike
from scipy.interpolate import CubicSpline

__all__ = ["load_grainsizes", "make_gsd_splines", "mu_from_cumul"]

PathLike = Union[str, Path]


def load_grainsizes(filename: PathLike) -> tuple[NDArray, NDArray]:
    """Load grainsizes from some text source
    TODO: This should be made more robust. Probably pull in pandas dependency.
    """
    f = Path(filename)
    data = np.loadtxt(f, skiprows=2, delimiter=',')
    xsieve = data[:, 0]
    gsd = data[:, 1:]
    if np.isclose(100, gsd[..., -1]).all():
        gsd /= 100
    return xsieve, gsd


def mu_from_cumul(xsieve: ArrayLike, gsd: ArrayLike) -> tuple[NDArray, NDArray]:
    """Computes the slope of the cumulative grainsize distribnution and also
    corresponding 'average' grain sizes for each sieve interval.

    Parameters
    ----------
    xsieve : ArrayLike
        Sieve sizes.
    gsd : ArrayLike
        Cumulative mass arranged so that `gsd[i]` is the cumulative mass that
        passed the sieve `xsieve[i]`.

    Returns
    -------
        tuple[NDArray, NDArray]
    Returns (`xmu`, `mu`) where `xmu` is at the center of log-scaled grainsize
    intervals.
    """
    xsieve, gsd = np.asarray(xsieve), np.asarray(gsd)
    xsieve = np.asarray(xsieve)
    x_mu = np.sqrt(xsieve[:-1] * xsieve[1:])
    mu = (gsd[1:] - gsd[:-1]) / (xsieve[1:] - xsieve[:-1])
    return x_mu, mu


def make_gsd_splines(
        xsieve: ArrayLike, gsd: ArrayLike, scale: str = 'log'
) -> tuple[Callable, Callable]:
    """

    Parameters
    ----------
    xsieve
    gsd
    scale

    Returns
    -------

    """
    if scale.lower() == 'log':
        spl = CubicSpline(np.log(xsieve), np.log(gsd))
        deriv = spl.derivative()

        def spl_gsd(x):
            return np.exp(spl(np.log(x)))

        def spl_mu(x):
            lgx = np.log(x)
            return np.exp(spl(lgx)) * deriv(lgx) / x

    elif scale.lower() == 'linear':
        spl_gsd = CubicSpline(xsieve, gsd)
        spl_mu = spl_gsd.derivative()

    else:
        raise ValueError(
            f'`scale` should be either `log` or `linear`, but got {scale=}.')

    return spl_gsd, spl_mu
