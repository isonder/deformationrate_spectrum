import numpy as np
from numpy.typing import NDArray

__all__ = ["powspace"]


def powspace(start: float, end: float, num: int) -> NDArray:
    """Numbers spaced evenly on a log scale.

        Parameters
        ----------
        start : float
            Start value of the sequence.
        end : float
            End value of the sequence.
        num : int
            Number of samples to generate.
        """
    if num < 2:
        raise ValueError(
            f'This function is meant to generate a range of several values, '
            f'not {num=}.'
        )
    if start <= 0. or end <= 0.:
        raise ValueError(
            f'xlogspace produces log spaced values larger 0. Please choose '
            f'start and end appropriately. Got {start=} and {end=}.'
        )
    return start * (end / start) ** (np.arange(num) / (num - 1))
