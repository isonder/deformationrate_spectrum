import numpy as np
from numpy.typing import NDArray, ArrayLike
from typing import Callable
from warnings import warn
from scipy.integrate import quad, quad_vec
from scipy.special import gamma

from deformationrate_spectrum import powspace

__all__ = ["ExpansionBase", "GradyBase", "WeibullBase", "make_mu"]


PI = np.pi
NAN = np.nan


def broadcast_y_q(func: Callable) -> Callable:
    """TODO: somehow merge this with broadcas_x_l
    A decorator which handles the array and scalar combinations of arguments
    `y` and `q` as they appear in the base functions and their integrals.
    """
    def broadcast(self, y: ArrayLike, q: ArrayLike) -> ArrayLike:
        y, q = np.asarray(y), np.asarray(q)
        if q.ndim > 1:
            raise ValueError(
                f'q should be a scalar or 1D-array-like, got {q.ndim=}.')
        return_scalar = y.ndim < 1 and q.ndim < 1
        unpack_last = y.ndim > 0 and q.ndim == 0
        y = y[..., None]
        ret = func(self, y, q)
        if return_scalar:
            return ret[0]
        elif unpack_last:
            return ret[..., 0]
        else:
            return ret
    return broadcast


def broadcast_x_l(func: Callable) -> Callable:
    """TODO: somehow merge this with broadcas_y_q"""

    # noinspection PyIncorrectDocstring
    def broadcast(instance, x: ArrayLike, l: ArrayLike) -> ArrayLike:
        """
        Parameters
        ----------
        x : ArrayLike
            Particle sizes.
        l : ArrayLike
            Expansion indeces. l=0, ..., len(lmdas).

        Returns
        -------
            ArrayLike
        `l`th component, evaluated at `x`.
        """
        x, l = np.asarray(x), np.asarray(l)
        return_scalar = x.ndim < 1 and l.ndim < 1
        unpack_last = x.ndim > 0 and l.ndim == 0
        x = x[..., None]
        ret = func(instance, x, l)
        if return_scalar:
            return ret[0]
        elif unpack_last:
            return ret[..., 0]
        else:
            return ret

    return broadcast


class ExpansionBase:
    """Base object of the expansion functions. This is not intended to be used
    directly.
    """
    empty_arr: NDArray = np.array(None)

    def __init__(
            self, q: float = 1, m0: float = 1.0, rho: float = 1.0,
            lmbmin: float = 1e-5, lmbmax: float = 1e-3, nterms: int = 20,
            formfac_v: float = PI / 6, formfac_s: float = PI
    ):
        """

        Parameters
        ----------
        q : float
            Common power for the expansion functions.
        m0 : float, optional
            Sample mass.
        rho : float, optional
            Sample mass density.
        lmbmin : float, optional
            Minimum of the expansion length scales.
        lmbmax : float, optional
            Maximum of the expansion length scale.
        nterms : int, optional
            Number of expansion terms.
        formfac_v : float, optional
            General (generic?) form factor for self-similar particles.
        formfac_s : float, optional
            Surface specific form factor for self-similar particles.
        """
        self.q: float = q
        self.m0: float = m0
        self.rho: float = rho
        self.lmbmin: float = lmbmin
        self.lmbmax: float = lmbmax
        self.nterms: int = nterms
        self.formfac_v: float = formfac_v
        self.formfac_s: float = formfac_s
        self.a_s: NDArray = np.full(self.nterms, 1 / self.nterms)
        self._nu0s: NDArray = self.empty_arr
        self._sig0s: NDArray = self.empty_arr
        self._mu0s: NDArray = self.empty_arr
        self.lmdas: NDArray = powspace(self.lmbmin, self.lmbmax, self.nterms)[::-1]
        self.ls: NDArray = np.arange(len(self.lmdas))

    def kernel(self, y: NDArray, q: NDArray) -> NDArray:
        raise NotImplemented

    @broadcast_y_q
    def nu_raw(self, y: NDArray, q: NDArray) -> NDArray:
        return self.kernel(y, q)

    @broadcast_y_q
    def sig_raw(self, y: NDArray, q: NDArray) -> NDArray:
        return y ** 2 * self.kernel(y, q)

    @broadcast_y_q
    def mu_raw(self, y: NDArray, q: NDArray) -> NDArray:
        return y ** 3 * self.kernel(y, q)

    def int2(self, q: ArrayLike) -> ArrayLike:
        if np.isscalar(q):
            ret = quad(lambda x: self.sig_raw(x, q), 0, np.inf)
        else:
            ret = quad_vec(lambda x: self.sig_raw(x, q), 0, np.inf)
        if ret[1] > 1e-8:
            warn(f"High error estimate ({ret[1]}) for {q=}.")
        return ret[0]

    def int3(self, q: ArrayLike) -> ArrayLike:
        if np.isscalar(q):
            ret = quad(lambda x: self.mu_raw(x, q), 0, np.inf)
        else:
            ret = quad_vec(lambda x: self.mu_raw(x, q), 0, np.inf)
        if ret[1] > 1e-8:
            warn(f"High error estimate ({ret[1]}) for {q=}.")
        return ret[0]

    @property
    def nu0s(self) -> NDArray:
        if self._nu0s is self.empty_arr:
            self._nu0s = self.m0 / (
                self.int3(self.q) * self.rho * self.formfac_v
                * self.lmdas ** 4
            )
        return self._nu0s

    @property
    def sig0s(self) -> NDArray:
        if self._sig0s is self.empty_arr:
            self._sig0s = self.m0 * self.formfac_s / (
                self.int3(self.q) * self.rho * self.formfac_v
                * self.lmdas ** 2
            )
        return self._sig0s

    @property
    def mu0s(self) -> NDArray:
        if self._mu0s is self.empty_arr:
            self._mu0s = self.m0 / (self.int3(self.q) * self.lmdas)
        return self._mu0s

    @broadcast_x_l
    def test_broadcast(self, x, l):
        return x ** l

    @broadcast_x_l
    def nu_l(self, x: NDArray, l: NDArray) -> NDArray:
        return self.nu0s[l] * self.nu_raw(x / self.lmdas[l], self.q)

    @broadcast_x_l
    def sig_l(self, x: NDArray, l: NDArray) -> NDArray:
        return self.sig0s[l] * self.sig_raw(x / self.lmdas[l], self.q)

    @broadcast_x_l
    def mu_l(self, x: NDArray, l: NDArray) -> NDArray:
        return self.mu0s[l] * self.mu_raw(x / self.lmdas[l], self.q)

    def integrate_mu_l(self, l: ArrayLike, xmin: float, xmax: float) -> ArrayLike:
        """Integrate the mu_l. Typically used to practically implement mass
        conservation as linear constraint for the minimizer in `Spectrum`. This
        is a generic method that uses a standard numeric quadrature. Subclasses
        for which such integrals are known analytically can override this method.

        Parameters
        ----------
        l : int or NDArray
            expansion index/indeces.
        xmin : float
            Start of integration interval.
        xmax : float
            End of integration interval.

        Returns
        -------
            float or NDArray
        Integrated mass value.
        """
        def func(x):
            return self.mu_l(x, l)
        if np.isscalar(l):
            ret = quad(func, xmin, xmax)[0]
        else:
            ret = quad_vec(func, xmin, xmax)[0]
        return ret

    def nu(self, x: ArrayLike) -> ArrayLike:
        return np.sum(self.a_s[None, :] * self.nu_l(x, self.ls), axis=1)

    def sig(self, x: ArrayLike) -> ArrayLike:
        return np.sum(self.a_s[None, :] * self.sig_l(x, self.ls), axis=1)

    def mu(self, x: ArrayLike) -> ArrayLike:
        return np.sum(self.a_s[None, :] * self.mu_l(x, self.ls), axis=1)

    def __len__(self):
        return self.nterms


class GradyBase(ExpansionBase):
    """...
    """
    def __init__(
            self, q: float = 1, m0: float = 1.0, rho: float = 1.0,
            lmbmin: float = 1e-5, lmbmax: float = 1e-3, nterms: int = 20,
            formfac_v: float = 1 / 6, formfac_s: float = PI
    ):
        ExpansionBase.__init__(
            self, q, m0, rho, lmbmin, lmbmax, nterms, formfac_v, formfac_s)

    def kernel(self, y: NDArray, q: NDArray) -> NDArray:
        return 1 / (1 + y ** q)


class WeibullBase(ExpansionBase):
    """...
    """
    def __init__(
            self, q: float = 1, m0: float = 1.0, rho: float = 1.0,
            lmbmin: float = 1e-5, lmbmax: float = 1e-3, nterms: int = 20,
            formfac_v: float = PI / 6, formfac_s: float = PI
    ):
        ExpansionBase.__init__(
            self, q, m0, rho, lmbmin, lmbmax, nterms, formfac_v, formfac_s)

    def kernel(self, y: NDArray, q: NDArray) -> NDArray:
        return q * y ** (q - 4) * np.exp(-y ** q)

    def int2(self, q: ArrayLike) -> ArrayLike:
        return gamma(1 - 1 / q)

    def int3(self, q: ArrayLike) -> ArrayLike:
        return 1.0 if np.isscalar(q) else np.ones_like(q, dtype=float)

    def integrate_mu_l(self, l: ArrayLike, xmin: float, xmax: float) -> ArrayLike:
        return np.exp(-(xmin / self.lmdas[l]) ** self.q) \
            - np.exp(-(xmax / self.lmdas[l]) ** self.q)


def make_mu(base: ExpansionBase) -> Callable:
    """Create a grain size distribution function from selected base.
    """
    def mu(x: ArrayLike, a: ArrayLike, q: float):
        assert np.isscalar(q), "mu: q needs to be a float valued scalar."
        base.a_s = a
        return np.sum(a[None, :] * base.mu_l(x, base.ls), axis=1)

    return mu
