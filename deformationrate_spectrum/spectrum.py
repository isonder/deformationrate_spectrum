from typing import Callable, Union, Tuple, Any
from numpy.typing import ArrayLike, NDArray
import numpy as np
from scipy.optimize import minimize, LinearConstraint, OptimizeResult
from scipy.integrate import quad

from deformationrate_spectrum import ExpansionBase, make_mu, powspace

__all__ = ['Spectrum']


class Spectrum:
    """Decompose a grainsize distribution into a spectrum of (narrow)
    fragmentation modes.
    """

    def __init__(
            self, target: Callable, base: ExpansionBase,
            xmin: float, xmax: float, npoints: int = 300
    ):
        """

        Parameters
        ----------
        target: Callable
            Target grainsize distribution function to be approximated. The
            function must accept one parameter, the grainsize(s).
        base : ExpansionBase
            The base function container used to approximate the grainsize
            distribution.
        xmin : float
            Low end of the grainsizes.
        xmax : float
            High end of the grainsizes.
        npoints : int
            Number of points to use for the approximation.
        """
        self.base: ExpansionBase = base
        self.target: Callable = target
        self.mu: Callable = make_mu(self.base)
        self.npoints: int = npoints
        self.xk: NDArray = powspace(xmin, xmax, self.npoints)
        self.muk: NDArray = self.target(self.xk)
        self.aconstr1: float = 1.  # factor for first difference constraint
        self.aconstr2: float = 1.  # factor for second difference constraint
        # noinspection PyTypeChecker
        self.dmass: float = quad(self.target, self.xk[0], self.xk[-1])[0]
        # Initial values of the expansion coeficients
        self.a0: NDArray = np.ones(len(self.base)) * self.dmass / len(self.base)

    def obfunc(self, a: NDArray) -> float:
        """Objective function to be minimized."""
        mmu = self.mu(self.xk, a, self.base.q)
        mmu[mmu <= 1e-128] = 1e-128
        return np.sum((np.log(mmu) - np.log(self.muk)) ** 2)

    @property
    def a(self) -> NDArray:
        return self.base.a_s

    @a.setter
    def a(self, a: NDArray) -> None:
        self.base.a_s = a

    @property
    def aok(self) -> NDArray:
        ret = np.zeros((len(self.base), len(self.base)), dtype=float)
        # noinspection PyTypeChecker
        ret[:, :] = self.base.integrate_mu_l(self.base.ls, self.xk[0], self.xk[-1])[None, :]
        return ret

    def mass_conservation_sonstraint(self) -> LinearConstraint:
        lmax: int = len(self.base)
        dm = np.full(lmax, self.dmass)
        return LinearConstraint(self.aok, dm - 1e-3, dm + 1e-3)

    def _primitives(self) -> Tuple[NDArray, NDArray, NDArray, NDArray]:
        lmax: int = len(self.base)
        zeros = np.zeros((lmax, lmax), dtype=float)
        ones = zeros + 1.
        i_s = np.arange(lmax)
        diag = zeros.copy()
        diag[i_s, i_s] = 1.
        upper_diag = zeros.copy()
        upper_diag[i_s[:-1], i_s[:-1] + 1] = 1.
        lower_diag = zeros.copy()
        lower_diag[i_s[1:], i_s[1:] - 1] = 1.
        return ones, diag, upper_diag, lower_diag

    def first_difference_constraints(self, factor: float) -> Tuple[LinearConstraint, ...]:
        """Computes four LinearConstraints for the minimizer that restrict the
        scatter from one result element to its neighbor to a factor times the
        average value of all result elements.

        Parameters
        ----------
        factor : float
            Scattering factor that determines the allowable changes between
            neighboring elements.

        Returns
        -------
            tuple
        A four tuple of `LinearConstraint`s with appropriately filled matrices.
        """
        lmax = len(self.base)
        ones, diag, upper_diag, lower_diag = self._primitives()
        return (
            LinearConstraint(
                ones * factor / lmax + diag - upper_diag,
                0, np.inf
            ),
            LinearConstraint(
                ones * factor / lmax - diag + upper_diag,
                0, np.inf
            ),
            LinearConstraint(
                ones * factor / lmax + diag - lower_diag,
                0, np.inf
            ),
            LinearConstraint(
                ones * factor / lmax - diag + lower_diag,
                0, np.inf
            )
        )

    def second_difference_constraints(self, factor: float) -> Tuple[LinearConstraint, ...]:
        """Computes four LinearConstraints for the minimizer that restrict the
        scatter from one result element to its neighbor to a factor times the
        average value of all result elements.

        Parameters
        ----------
        factor : float
            Scattering factor that determines the allowable changes between
            neighboring elements.

        Returns
        -------
            tuple
        A four tuple of `LinearConstraint`s with appropriately filled matrices.
        """
        lmax = len(self.base)
        ones, diag, upper_diag, lower_diag = self._primitives()
        return (
            LinearConstraint(
                ones * factor / lmax - upper_diag + 2 * diag - lower_diag,
                0, np.inf
            ),
            LinearConstraint(
                ones * factor / lmax + upper_diag - 2 * diag + lower_diag,
                0, np.inf
            )
        )

    def find_spectrum(
            self, options: dict, ret: bool = True
    ) -> Union[None, OptimizeResult]:
        """Find the deformation spectrum.

        Parameters
        ----------
        options : dict, optional
            Options parsed to the minimize method. Documented at scipy:
            https://docs.scipy.org/doc/scipy/reference/optimize.minimize-trustconstr.html
        ret : bool, optional
            Whether to return the minimizer's result object.
        """
        lmax: int = len(self.base)
        bnds = np.zeros((lmax, 2), dtype=int)
        bnds[:, 1] = 1
        constr = (
            # LinearConstraint(diag, 0, np.inf),
            self.mass_conservation_sonstraint(),
        ) \
            + self.first_difference_constraints(self.aconstr1) \
            + self.second_difference_constraints(self.aconstr2)
        res = minimize(
            fun=self.obfunc,
            x0=self.a0,
            method='trust-constr',
            bounds=bnds,
            constraints=constr,
            options=options
        )
        self.a[:] = res['x']
        if ret:
            return res
